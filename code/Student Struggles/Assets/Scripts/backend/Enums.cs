using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enums
{
    public enum TimeOfDay
    {
        MORNING = 0,
        NOON = 1,
        EVENING = 2,
        NIGHT = 3
    }

    public enum Weekday
    {
        MONDAY = 0,
        TUESDAY = 1,
        WEDNESDAY = 2,
        THURSDAY = 3,
        FRIDAY = 4,
        SATURDAY = 5,
        SUNDAY = 6
    }

    public enum ActionType
    {
        STUDY,
        HOBBIES,
        WORKOUT,
        WORK,
        MEET_FRIENDS,
        CONFERENCE
    }
}
