using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomEvent : StudentAction
{
    [SerializeField]
    private float spawnChance = 1f;

    public float getSpawnChance() => spawnChance;
}
