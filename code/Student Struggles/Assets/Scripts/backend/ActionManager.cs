using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ActionManager : MonoBehaviour
{
    GameManager gameManager = null;
    UIManager uiManager = null;
    Semester semester = null;

    [SerializeField]
    private GameObject canvas;
    private const float BUTTON_DISTANCE = 150.0f;
    [SerializeField]
    private Button buttonPrefab;
    [SerializeField]
    private GameObject eventButtonPrefab;

    [SerializeField]
    private List<StudentAction> actionList = new List<StudentAction>();
    [SerializeField]
    private List<RandomEvent> eventList = new List<RandomEvent>();
    private List<GameObject> activeButtons = new List<GameObject>();

    private void Start()
    {
        gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
        uiManager = FindObjectOfType<UIManager>();
    }

    private void Update()
    {
        if (semester == null)
        {
            semester = FindObjectOfType<Semester>();
        }
    }

    private List<StudentAction> SelectAvailableActions()
    {
        List<StudentAction> results = new List<StudentAction>();
        if (actionList.Count > 0)
        {
            foreach (StudentAction action in actionList)
            {
                if (action.Conditions())
                {
                    results.Add(action);
                }
            }
        }
        return results;
    }

    private List<RandomEvent> SelectRandomEvents()
    {
        List<RandomEvent> results = new List<RandomEvent>();
        if (eventList.Count > 0)
        {
            float rng = 0;
            foreach (var randomEvent in eventList)
            {
                if (randomEvent.Conditions() && randomEvent.getSpawnChance() >= rng)
                {
                    results.Add(randomEvent);
                }
            }
        }
        return results;
    }

    public void CreateChoiceButtons()
    {
        CreateRandomEventButtons();
        CreateActionButtons();
    }

    private void CreateRandomEventButtons()
    {
        List<RandomEvent> randomEvents = SelectRandomEvents();
        int index = 0;
        foreach (RandomEvent rEvent in randomEvents)
        {
            int modifier = 1;
            if (index % 2 == 1)
            {
                modifier = -1;
            }
            double x = index / 2;
            float dist = (float)Math.Ceiling(x);

            float y = 0 + modifier * dist * BUTTON_DISTANCE;
            GameObject eventPanel = Instantiate(eventButtonPrefab, canvas.transform);
            activeButtons.Add(eventPanel);
            eventPanel.transform.localPosition = new Vector3(-675, y, 0);
            foreach (Transform child in eventPanel.transform)
            {
                if (child.gameObject.name == "AcceptButton")
                {
                    child.gameObject.GetComponent<Button>().onClick.AddListener(() => DoAction(rEvent));
                }

                if (child.gameObject.name == "RefuseButton")
                {
                    child.gameObject.GetComponent<Button>().onClick.AddListener(() => CloseEvent(eventPanel));
                }
            }
        }
    }

    private void CreateActionButtons()
    {
        List<StudentAction> availableActions = SelectAvailableActions();
        int index = 1;
        foreach (StudentAction action in availableActions)
        {
            int modifier = 1;
            if (index % 2 == 1)
            {
                modifier = -1;
            }
            double x = index / 2;
            float dist = (float)Math.Ceiling(x);

            float y = 0 + modifier * dist * BUTTON_DISTANCE;
            Button actionButton = Instantiate(buttonPrefab, canvas.transform);
            activeButtons.Add(actionButton.gameObject);
            actionButton.transform.localPosition = new Vector3(-0, y, 0);
            actionButton.onClick.AddListener(() => DoAction(action));
            TMP_Text buttonText = actionButton.GetComponentInChildren<TMP_Text>();
            buttonText.text = action.text[0];
            index++;
        }
    }

    private void CleanupButtons()
    {
        foreach(GameObject g in activeButtons)
        {
            Destroy(g);
        }
    }

    public void DoAction(StudentAction action)
    {
        if (!gameManager.inResultsScreen)
        {
            CleanupButtons();
            gameManager.SetTurnPlayed(true);
            semester.AssignPoints(action.getPoints());
            uiManager.ShowTextBox();
            uiManager.SetTextBoxContent(action.text[1]);
        }
    }

    public void CloseEvent(GameObject eventBox)
    {
        activeButtons.Remove(eventBox);
        Destroy(eventBox);
    }

    public void HideButtons()
    {
        foreach (GameObject g in activeButtons)
        {
            g.SetActive(false);
        }
    }
    public void ShowButtons()
    {
        foreach (GameObject g in activeButtons)
        {
            g.SetActive(true);
        }
    }
}
