using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class ConditionLibrary : MonoBehaviour
{
    public static bool GetCondition(ActionType actionType)
    {
        Semester semester = FindObjectOfType<Semester>();
        Student student = FindObjectOfType<Student>();
        GameManager gm = FindObjectOfType<GameManager>();

        bool x = false;
        switch (actionType)
        {
            case ActionType.STUDY:
                // available when not in the evening or on the weekend or on sunday night
                x = !(MatchTimeOfDay(gm, TimeOfDay.EVENING) || (MatchWeekday(gm, Weekday.SATURDAY) && MatchTimesOfDay(gm, new List<TimeOfDay>() { TimeOfDay.MORNING, TimeOfDay.NIGHT })) || (MatchWeekday(gm, Weekday.SUNDAY) && MatchTimeOfDay(gm, TimeOfDay.MORNING)));
                break;
            case ActionType.HOBBIES:
                // available when in the morning or at night
                x = MatchTimesOfDay(gm, new List<TimeOfDay>() { TimeOfDay.MORNING, TimeOfDay.NIGHT});
                break;
            case ActionType.WORKOUT:
                // available in mornings, evenings or on saturday nights
                x = (MatchTimesOfDay(gm, new List<TimeOfDay>() {TimeOfDay.MORNING, TimeOfDay.EVENING}) || MatchDayAndTime(gm, Weekday.SATURDAY, TimeOfDay.NIGHT));
                break;
            case ActionType.WORK:
                x = MatchTimesOfDay(gm, new List<TimeOfDay>() { TimeOfDay.NOON, TimeOfDay.EVENING });
                break;
            case ActionType.MEET_FRIENDS:
                // available in evenings or on friday or saturday nights
                x = (MatchTimeOfDay(gm, TimeOfDay.EVENING) || MatchDayAndTime(gm, Weekday.FRIDAY, TimeOfDay.NIGHT) || MatchDayAndTime(gm, Weekday.SATURDAY, TimeOfDay.NIGHT));
                break;
            case ActionType.CONFERENCE:
                // 
                x = (true);
                break;
        }
        return x;
    }

    private static bool MatchTimeOfDay(GameManager gm, TimeOfDay time)
    {
        bool b = gm.GetTimeOfDay().Equals(time);
        return b;
    }
    private static bool MatchWeekday(GameManager gm, Weekday day)
    {
        bool b = gm.GetDayOfWeek().Equals(day);
        return b;
    }
    private static bool MatchWeekdays(GameManager gm, List<Weekday> days)
    {
        foreach (Weekday day in days)
        {
            if (MatchWeekday(gm, day)) return true;
        }
        return false;
    }
    private static bool MatchTimesOfDay(GameManager gm, List<TimeOfDay> time)
    {
        foreach(TimeOfDay timeOfDay in time)
        {
            if (MatchTimeOfDay(gm, timeOfDay)) return true;
        }
        return false;
    }
    private static bool MatchDayAndTime(GameManager gm, Weekday day, TimeOfDay time)
    {
        return gm.GetDayOfWeek().Equals(day) && gm.GetTimeOfDay().Equals(time);
    }
}
