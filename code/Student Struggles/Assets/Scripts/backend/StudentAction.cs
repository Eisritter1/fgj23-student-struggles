using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class StudentAction : MonoBehaviour
{
    [SerializeField]
    protected int[] points = new int[5];
    [SerializeField]
    public List<string> text = new List<string>();          // public for testing; set back to private before build.
    [SerializeField]
    private ActionType actionType;

    public int[] getPoints() => points;
    public bool Conditions()
    {
        return ConditionLibrary.GetCondition(actionType);
    }
}
