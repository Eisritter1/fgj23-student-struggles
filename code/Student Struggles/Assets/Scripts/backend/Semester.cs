using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Semester : MonoBehaviour
{
    private int currentSemester = 1;
    private int[] points = new int[5];
    private const int MAX_SEMESTER = 8;
    Student student;

    private void Start()
    {
        student = GetComponent<Student>();
    }

    public void AssignPoints(int[] newPoints)
    {
        if (newPoints.Length != points.Length)
        {
            return;
        }
        for (int i = 0; i < points.Length; i++)
        {
            points[i] += newPoints[i];
        }
    }

    private void ResetData()
    {
        points = new int[5];
    }

    public int[] GetPoints() => points;
    public int GetCurrentSemester() => currentSemester;
    public bool GraduatesNextSemester()
    {
        if (currentSemester + 1 > MAX_SEMESTER)
        {
            return true;
        }
        // skip to end screen
        return false;
    }
    public void IncrementSemester() => currentSemester++;

    public void ProgressToNextSemester()
    {
        student.ModifyValues(points);
        IncrementSemester();
        ResetData();
    }
}
