using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using UnityEngine;

public class Student : MonoBehaviour
{
    private float[] points = new float[5] { 2.7f, 0.5f, 1.0f, 0.5f, 0.5f }; 

    public float[] GetPoints() => points;

    public void ModifyValues(int[] semesterPoints)
    {
        if (semesterPoints.Length != points.Length)
        {
            UnityEngine.Debug.LogError("Provided array invalid. Length does not match student array's length (5)");
            return;
        }

        // Modify grades
        float newGrade = Clamp(points[0] - (semesterPoints[0]/10), 1.0f, 5.0f);
        points[0] = newGrade;

        // Modify mood
        float newMood = Clamp(points[1] + ((float)semesterPoints[1] / 100));
        points[1] = newMood;

        // Modify health
        float newHealth = Clamp(points[2] + 2 * ((float)semesterPoints[2] / 100));
        points[2] = newHealth;

        // Modify wealth
        float newWealth = Clamp(points[3] + ((float)semesterPoints[3] / 100));
        points[3] = newWealth;

        // Modify connections
        float newFriends = Clamp(points[4] + 3 * ((float)semesterPoints[4] / 100));
        points[4] = newFriends;
    }

    float Clamp(float value, float lowBound = 0.0f, float topBound = 1.0f)
    {
        if (value < lowBound)
        {
            value = lowBound;
        }
        if (value > topBound)
        {
            value = topBound;
        }
        return value;
    }
}
