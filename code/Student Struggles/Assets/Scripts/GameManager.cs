using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class GameManager : MonoBehaviour
{
    TimeOfDay time = TimeOfDay.MORNING;
    Weekday today = Weekday.MONDAY;
    private bool turnPlayed = false;
    private bool gameOver = false;
    public bool inResultsScreen = false;
    [SerializeField]
    Sprite[] backgroundSprites;
    [SerializeField]
    GameObject background;

    Semester semester = null;
    ActionManager actionManager = null;
    UIManager uiManager = null;

    private void Start()
    {
        semester = FindObjectOfType<Semester>();
        actionManager = gameObject.GetComponent<ActionManager>();
        uiManager = FindObjectOfType<UIManager>();
        StartDayCycle();
    }

    public void StartDayCycle()
    {
        TimeOfDay currentTimePeriod = TimeOfDay.MORNING;
        Weekday dayOfWeek = Weekday.MONDAY;
        UpdateSprites();
        ShowDayTasks();
    }

    public void UpdateDayCycle() {
        if (turnPlayed && !gameOver)
        {
            turnPlayed = false;
            uiManager.HideTextBox();
            if (time == TimeOfDay.NIGHT)
            {
                if (today == Weekday.SUNDAY)
                {
                    ResetAndUpdateTimePeriods();
                    ShowDayTasks();
                }
                else
                {
                    time = TimeOfDay.MORNING;
                    today = (Weekday)(((int)today + 1) % Enum.GetNames(typeof(Weekday)).Length);
                    ShowDayTasks();
                }
            }
            else
            {
                time = (TimeOfDay)(((int)time + 1) % Enum.GetNames(typeof(TimeOfDay)).Length);
                ShowDayTasks();
            }
            UpdateSprites();
        }
    }

    private void ResetAndUpdateTimePeriods() {
        time = TimeOfDay.MORNING;
        today = Weekday.MONDAY;
        if (semester.GraduatesNextSemester())
            EndGame();
        else {
            ShowSemesterStats();
        }
    }

    private void ShowDayTasks() {
        if (!inResultsScreen)
        {
            actionManager.CreateChoiceButtons();
        }
    }

    private void ShowSemesterStats() {
        // Call Semester Stats Screen
        inResultsScreen = true;
        uiManager.OpenSemesterStatsPanel();
        //actionManager.HideButtons();
        Debug.Log("Ended Semester " + (semester.GetCurrentSemester()));
    }

    public void StartNewSemester()
    {
        inResultsScreen = false;
        //actionManager.ShowButtons();
        semester.ProgressToNextSemester();
        ShowDayTasks();
    }

    private void EndGame() {
        // Call Study Stats Screen
        gameOver = true;
        Debug.Log("Completed Studies!");
    }

    public TimeOfDay GetTimeOfDay() => time;
    public Weekday GetDayOfWeek() => today;
    public void SetTurnPlayed(bool b) => turnPlayed = b;

    private void UpdateSprites()
    {
        int i = (int) time;
        background.GetComponent<SpriteRenderer>().sprite = backgroundSprites[i];
        uiManager.UpdatePhone(time, today);
    }
}