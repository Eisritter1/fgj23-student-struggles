using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Enums;

public class UIManager : MonoBehaviour
{
    // Variables:
    private Student player;
    private GameManager gm; 

    // Student overview:
    [SerializeField]
    private GameObject overviewPanel;
    [SerializeField]
    private GameObject grades;
    private float lastKnownGrade;
    private Slider gradeSlider;
    private TMP_Text gradeValueText;
    [SerializeField]
    private GameObject mood;
    private float lastMood;
    private Slider moodSlider;
    private TMP_Text moodValueText;
    [SerializeField]
    private GameObject health;
    private float lastHealth;
    private Slider healthSlider;
    private TMP_Text healthValueText;
    [SerializeField]
    private GameObject wealth;
    private float lastWealth;
    private Slider wealthSlider;
    private TMP_Text wealthValueText;
    [SerializeField]
    private GameObject social;
    private float lastSocial;
    private Slider socialSlider;
    private TMP_Text socialValueText;

    // Text box:
    [SerializeField]
    private GameObject textBox;

    // Phone:
    [SerializeField]
    GameObject phone;
    private TMP_Text dayOfWeek;
    [SerializeField]
    private Sprite[] phoneSprites;

    // Semester stats:
    [SerializeField]
    private GameObject semesterStatsPanel;
    Button semesterButton;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Student>();
        gm = FindObjectOfType<GameManager>();

        gradeSlider = grades.GetComponentInChildren<Slider>();
        lastKnownGrade = gradeSlider.value;
        foreach (Transform c in grades.transform)
        {
            if (c.name == "Value")
            {
                gradeValueText = c.gameObject.GetComponent<TMP_Text>();
            }
        }

        moodSlider = mood.GetComponentInChildren<Slider>();
        lastMood = moodSlider.value;
        foreach (Transform c in mood.transform)
        {
            if (c.name == "Value")
            {
                moodValueText = c.gameObject.GetComponent<TMP_Text>();
            }
        }

        healthSlider = health.GetComponentInChildren<Slider>();
        lastHealth = healthSlider.value;
        foreach (Transform c in health.transform)
        {
            if (c.name == "Value")
            {
                healthValueText = c.gameObject.GetComponent<TMP_Text>();
            }
        }

        wealthSlider = wealth.GetComponentInChildren<Slider>();
        lastWealth = wealthSlider.value;
        foreach (Transform c in wealth.transform)
        {
            if (c.name == "Value")
            {
                wealthValueText = c.gameObject.GetComponent<TMP_Text>();
            }
        }

        socialSlider = social.GetComponentInChildren<Slider>();
        lastSocial = socialSlider.value;
        foreach (Transform c in social.transform)
        {
            if (c.name == "Value")
            {
                socialValueText = c.gameObject.GetComponent<TMP_Text>();
            }
        }

        dayOfWeek = phone.GetComponentInChildren<TMP_Text>();

        foreach (Transform c in semesterStatsPanel.transform)
        {
            if (c.name == "Continue")
            {
                semesterButton = c.gameObject.GetComponent<Button>();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdateStudentOverview();
    }

    public void HideStudentOverview() => overviewPanel?.SetActive(false);
    public void ShowStudentOverview() => overviewPanel?.SetActive(true);
    public void HideTextBox() => textBox?.SetActive(false);
    public void ShowTextBox() => textBox?.SetActive(true);

    void UpdateStudentOverview()
    {
        float[] points = player.GetPoints();
        UpdateGrades(points[0]);
        UpdateMood(points[1]);
        UpdateHealth(points[2]);
        UpdateWealth(points[3]);
        UpdateSocial(points[4]);
    }
    void UpdateGrades(float grade)
    {
        if (lastKnownGrade != grade)
        {
            gradeSlider.value = grade;
            gradeValueText.text = grade.ToString();
            lastKnownGrade = grade;
        }
    }
    void UpdateMood(float mood)
    {
        if(lastMood != mood)
        {
            moodSlider.value = mood;
            moodValueText.text = (mood*100).ToString() + "%";
            lastMood = mood;
        }
    }
    void UpdateHealth(float health)
    {
        if (lastHealth != health)
        {
            healthSlider.value = health;
            healthValueText.text = (health * 100).ToString() + "%";
            lastHealth = health;
        }
    }
    void UpdateWealth(float wealth)
    {
        if (lastWealth != wealth)
        {
            wealthSlider.value = wealth;
            wealthValueText.text = (wealth * 100).ToString() + "%";
            lastWealth = wealth;
        }
    }
    void UpdateSocial(float social)
    {
        if (lastSocial != social)
        {
            socialSlider.value = social;
            socialValueText.text = (social * 100).ToString() + "%";
            lastSocial = social;
        }
    }

    public void UpdatePhone(TimeOfDay time, Weekday day)
    {
        Image im = phone.GetComponent<Image>();
        int i = (int) time;
        im.sprite = phoneSprites[i];

        string text = "";
        switch (day)
        {
            case Weekday.MONDAY:
                text = "MON";
                break;
            case Weekday.TUESDAY:
                text = "TUE";
                break;
            case Weekday.WEDNESDAY:
                text = "WED";
                break;
            case Weekday.THURSDAY:
                text = "THU";
                break;
            case Weekday.FRIDAY:
                text = "FRI";
                break;
            case Weekday.SATURDAY:
                text = "SAT";
                break;
            case Weekday.SUNDAY:
                text = "SUN";
                break;
        }
        if (dayOfWeek == null)
        {
            dayOfWeek = phone.GetComponentInChildren<TMP_Text>();
        }
        dayOfWeek.text = text;
    }

    public void SetTextBoxContent(string s)
    {
        textBox.GetComponentInChildren<TMP_Text>().text = s;
    }

    public void OpenSemesterStatsPanel()
    {
        HideNonSemesterStatUI();
        semesterStatsPanel.SetActive(true);
        UpdateSemesterStatText();
    }

    private void UpdateSemesterStatText()
    {
        TMP_Text title = null;
        TMP_Text text = null;
        foreach (Transform t in semesterStatsPanel.transform)
        {
            if (t.name == "Overview")
            {
                text = t.gameObject.GetComponent<TMP_Text>();
            }
            if (t.name == "Title")
            {
                title = t.gameObject.GetComponent<TMP_Text>();
            }
        }
        Semester semester = FindObjectOfType<Semester>();

        int currentSem = semester.GetCurrentSemester();
        if (currentSem >= 8) 
        {
            title.text = "Congratulations, you have completed your studies!";
        }
        else
        {
            title.text = "Completed Semester " + currentSem + "!";
        }
        
        int[] semesterPoints = semester.GetPoints();
        float[] studentStatus = player.GetPoints();
        List<string> textList = new List<string>();
        float spdiff = (float)semesterPoints[0] / 10;
        float mpdiff = (float)semesterPoints[1] / 100;
        float hpdiff = 2 * ((float)semesterPoints[2] / 100);
        float wpdiff = (float)semesterPoints[3] / 100;
        float cpdiff = 3 * ((float)semesterPoints[2] / 100);
        textList.Add("You have completed Semester " + semester.GetCurrentSemester() + ", here's what you achieved!");
        textList.Add("You have scored " + semesterPoints[0] + " points towards your Studies, gaining " + spdiff + " on your grade!");
        textList.Add("You have scored " + semesterPoints[1] + " points towards your Mood, gaining " + mpdiff + "% mood!");
        textList.Add("You have scored " + semesterPoints[2] + " points towards your Health, gaining " + hpdiff + "% health!");
        textList.Add("You have scored " + semesterPoints[3] + " points towards your Finances, gaining " + wpdiff + "% money!");
        textList.Add("You have scored " + semesterPoints[4] + " points towards your Social life, gaining " + cpdiff + "% social contacts!");
        string fullText = string.Join("\n", textList);
        text.text = fullText;
    }
    public void CloseSemesterStatsPanel()
    {
        Semester semester = FindObjectOfType<Semester>();
        if (semester.GetCurrentSemester() +1 < 9)
        {
            OpenNonSemesterStatUI();
            semesterStatsPanel.SetActive(false);
            gm.StartNewSemester();
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        }
    }

    void HideNonSemesterStatUI()
    {
        Semester semester = FindObjectOfType<Semester>();
        if (semester.GetCurrentSemester() + 1 < 9)
        {
            overviewPanel.SetActive(false);
            // rename button to return to main menu
            semesterButton.GetComponentInChildren<TMP_Text>().text = "Return to main menu";
        }
        textBox.SetActive(false);
        phone.SetActive(false);
    }
    void OpenNonSemesterStatUI()
    {
        overviewPanel.SetActive(true);
        phone.SetActive(true);
    }
}
