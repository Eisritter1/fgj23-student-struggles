using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleMovemnet : MonoBehaviour
{
    [SerializeField] private float amp;
    [SerializeField] private float freq;
    private Vector3 initialPos;

    private void Start()
    {
            initialPos = transform.localPosition;
    }

    private void Update()
    {
        transform.localPosition = new Vector3(initialPos.x, Mathf.Sin(Time.time * freq) * amp + initialPos.y, initialPos.z);
    }
}
